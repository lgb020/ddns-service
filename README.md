# ddns-service

#### 介绍
使用Java编写的域名更新程序，如果你有家用的公网IP，并且IP不固定时，可以使用本项目程序，为你的域名动态更新IP，目前适配了腾讯云DNSpod的接口。其他云厂商待完善。  
简单易用、轻量级，实测运行占用内存在 70MB 左右。



#### 使用说明
1.  clone项目到本地  
2.  创建云厂商的访问Token，腾讯云例如 DNSpod -> API密匙 -> DNS token -> 新建DNS token，并保存好token ID、token  
3.  修改 resources/application.properties配置文件，将其中的域名、token ID、token换成你的即可  
4.  maven打包后会在项目的target目录生成ddns-service-1.0.0-jar-with-dependencies.jar文件。将项目部署并运行，默认3分钟动态更新一次，若IP无变化则不会更新，可通过日志文件查看，运行后会在jar文件当前目录生成ddnsRun.log日志文件



