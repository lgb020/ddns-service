package com.xh1024.demo.ddns.ddnsservice;

import com.xh1024.demo.ddns.ddnsservice.config.DDNSTask;
import com.xh1024.demo.ddns.ddnsservice.utils.LogUtil;


public class DdnsServiceApplication {


    public static void main(String[] args) {


        try {

            while (true){
                DDNSTask.TencentDDNSTask();
                Thread.sleep(3*60*1000);//每隔3分钟执行一次
            }

        }catch (Exception e){
            e.printStackTrace();
            LogUtil.writeErrorLog(e.getMessage());

        }
    }

}
