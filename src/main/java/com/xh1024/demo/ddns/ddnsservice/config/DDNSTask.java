package com.xh1024.demo.ddns.ddnsservice.config;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xh1024.demo.ddns.ddnsservice.utils.HttpUtil;
import com.xh1024.demo.ddns.ddnsservice.utils.LogUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;


/**
 * @description: 腾讯云DDNS定时任务
 * @author: wang zhen
 * @create: 2020/05/20
 */


public class DDNSTask {

    private static ResourceBundle bundle = ResourceBundle.getBundle("application");

    public static final String IP_QUERY_DOMAIN=bundle.getString("ip-query-domain");

    //DNSPod-Token的ID
    public static final String DNSPOD_TOKEN_ID=bundle.getString("dnspod-token-id");

    //在DNSPod-Token的Token
    public static final String DNSPOD_TOKEN=bundle.getString("dnspod-token");

    //要做修改的二级域名
    public static final String DOMAIN=bundle.getString("dnspod-domain");

    //主机记录名，例如www.baidu.com的主机记录名是www，home.abc.com的主机记录名是home，只想修改主域名可填写@
    public static final String HOME=bundle.getString("dnspod-domain-home");





    public static void TencentDDNSTask() {

        try {

            //1.查询域名信息
            Map<String,String> params1=new HashMap<String, String>();
            params1.put("domain",DOMAIN);
            params1.put("record_type","A");
            params1.put("login_token",DNSPOD_TOKEN_ID+","+DNSPOD_TOKEN);//接口需要tokenID+token组合发送
            params1.put("format","json");
            String post = HttpUtil.post("https://dnsapi.cn/Record.List", null, params1, "utf-8");
            JSONObject res = JSONObject.parseObject(post);
            JSONObject statusRes = JSONObject.parseObject(res.getString("status"));

            //如果请求失败，打印失败信息
            if(!statusRes.getString("code").equals("1")){
                LogUtil.writeErrorLog(statusRes.getString("message"));
                return;
            }


            //因为查询的是二级域名，会返回该域名的所有子域名，需要找出要修改的那条记录
            JSONArray records = JSONArray.parseArray(res.getString("records"));
            JSONObject record=null;
            for(int i=0;i<records.size();i++){
                if(records.getJSONObject(i).getString("name").equals(HOME)){
                    record=records.getJSONObject(i);
                    break;
                }
            }


            if(record==null){
                LogUtil.writeErrorLog("暂无"+HOME+"相关的域名记录");
                return;
            }


            //获取当前最新IP
            String ip = HttpUtil.get(IP_QUERY_DOMAIN, null, null);



            //2.判断IP是否一致，若一致则不执行修改
            String oldIp=record.getString("value");
            if(oldIp.equals(ip)){
                LogUtil.writeInfoLog("IP一致，无需修改");
                return;
            }


            //3.不一致，执行修改
            Map<String,String> params2=new HashMap<String, String>();
            params2.put("domain",DOMAIN);
            params2.put("record_id",record.getString("id"));
            params2.put("record_line_id",record.getString("line_id"));
            params2.put("login_token",DNSPOD_TOKEN_ID+","+DNSPOD_TOKEN);//接口需要tokenID+token组合发送
            params2.put("value",ip);
            String resJson = HttpUtil.post("https://dnsapi.cn/Record.Ddns", null, params2, "utf-8");

            //如果请求失败，打印失败信息
            JSONObject jsonObject = JSONObject.parseObject(resJson);
            JSONObject statusRes2 = JSONObject.parseObject(jsonObject.getString("status"));
            if(!statusRes2.getString("code").equals("1")){
                LogUtil.writeErrorLog(statusRes2.getString("message"));
                return;
            }

            LogUtil.writeInfoLog("已修改为最新IP:"+ip);


        }catch (Exception e){
            LogUtil.writeErrorLog(e.getMessage());
        }
    }
}
