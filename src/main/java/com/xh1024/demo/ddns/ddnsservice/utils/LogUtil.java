package com.xh1024.demo.ddns.ddnsservice.utils;

import java.io.FileWriter;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * @Description:
 * @Author wangzhen
 * @Date 2022-08-25 05:59
 **/
public class LogUtil {

    private static ResourceBundle bundle = ResourceBundle.getBundle("application");

    //日志文件位置
    public static final String LOG_FILE_PATH=bundle.getString("log-file-path");


    public static void writeInfoLog(String message){

        try (FileWriter fileWriter=new FileWriter(LOG_FILE_PATH,true)){
            String msg= DateUtil.formatDateTime(new Date()) +" info->"+message+"\n";
            fileWriter.write(msg);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void writeErrorLog(String message){
        try (FileWriter fileWriter=new FileWriter(LOG_FILE_PATH,true)){
            String msg= DateUtil.formatDateTime(new Date()) +" error->"+message+"\n";
            fileWriter.write(msg);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
